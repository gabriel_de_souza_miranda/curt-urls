<!doctype html>
<html lang="pt-br">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title><?php echo $titulo ?></title>

    <!-- icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">

    <!-- FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,500;1,400&display=swap" rel="stylesheet">

    <!-- CSS para footer index.php-->
  


</head>

<body>

    <!--MENU-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="<?php echo base_url("user/index") ?>">CurtUrl's</a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <!--<a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>-->
                </li>
            </ul>
        </div>

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="<?php echo base_url("user/login") ?>">
                    <button type="button" class="btn btn-outline-info" style="color:white;">Login</button>
                </a>

                <a href="<?php echo base_url("user/cadastro") ?>">
                    <button type="button" class="btn btn-outline-info" style="color:white;">Cadastrar-se</button>
                </a>
            </li>
        </ul>
    </nav>
    <!--MENU-->