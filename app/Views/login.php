<?php
//header quando o usuario vai fazer login
echo $this->include('includes/header_login', array('titulo' => $titulo));

//css da pagina
echo $this->include('includes/style');

//footer padrão
echo $this->include('includes/footer');

?>
<!--gambiarra-->

<br><br>

<div class="container-sm card border border-dark" style="width: 28rem;">

    <br>
    <h3 class="text-center">Login de Usuário</h3>
    <br>

    <form id="inline" method="post" action="<?php echo base_url("user/inserir") ?>">
        <div class="form-label-group">
            <input type="text" class="form-control border border-dark" placeholder="Email" id="email" name="email">
            <label for="email"></label>
        </div>

        <div class="form-label-group">
            <input type="password" class="form-control border border-dark" placeholder="Senha" id="senha" name="senha">
            <label for="senha"></label>
        </div>

        <button type="submit" class="btn btn-dark col border border-success">Entrar</button>
        
        <br>
        <br>
        
        <p class="text-right">
            <a href="<?php echo base_url("user/#") ?>">Esqueci a senha</a>
        </p>
    </form>
</div>