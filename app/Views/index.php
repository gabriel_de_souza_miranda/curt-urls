<?php
//header
echo $this->include('includes/header_login', array('titulo' => $titulo));

//css da pagina
echo $this->include('includes/style');

?>

<!--<center><img src="<?php echo base_url('imagens/curtUrl.jpeg')  ?>" style="width:50% " > </center> -->



<p class="h1" style="padding:50px 10px 0px 50px;font-weight:bold; font-size:40px">CurtUrl's</p>
<p class="h5" style="padding:10px 10px 0px 50px; font-size:20px">
  Um encurtador de URL desenvolvido para compactar <br>o tamanhos de seus links
</p>




<br><br>
<center>
  <h2 style="font-weight:bold">Cole a URL a ser encurtada</h2>
</center>
<div class="d-flex justify-content-center">

  <form class="form-inline">

    <div class="form-group mx-sm-2 mb-2">
      <label for="inputPassword2" class="sr-only">Password</label>
      <input type="password" class="form-control border border-dark" id="inputPassword2" placeholder="Encurte o seu link" size="60">
    </div>
    <button type="submit" class="btn btn-primary mb-2 border border-dark">Encurtar</button>
  </form>
</div>
<br><br><br>

<div class="p-2 mb-0 bg-dark text-white">
  <center>
    <h1 style="padding:50px 10px 0px 50px;font-weight:bold; font-size:35px">Cadastre-se no CurtUrl's - Benefícios:</h1>
  </center>


  <footer class="page-footer font-small teal pt-4">

    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 mt-3">

          <!-- Content -->

          <h5 class="text-uppercase font-weight-bold">Gestão de url's</h5>
          <p> Permite visualizar a quantidade de acesso aos links encurtadados por você </p>

        </div>
        <!-- Grid column -->

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
        <div class="col-md-6 mb-md-0 mb-3">

          <!-- Content -->
          <h5 class="text-uppercase font-weight-bold">histórico</h5>
          <p> Permite visualizar um histórico completo com todos as url's encurtadas </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 mb-md-0 mb-3">

          <!-- Content -->
          <h5 class="text-uppercase font-weight-bold">Compartilhamento</h5>
          <p> Permite compartilhar todas as url's encurtadas pelo o usuário enquanto logado </p>

        </div>
        <!-- Grid column -->


      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Text -->


    <div class="fixar_pag">
    © <?php $hoje = date('Y');
      echo $hoje; ?> Copyright: Abdiel & Gabriel & Weslley
  </div>
  </footer>
  <!-- Footer -->
  
</div>








<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
</body>

</html>