<?php namespace App\Controllers;

class User extends BaseController
{

	public function index()
	{
		$data ['titulo'] = "CurtURL's - Pagina Inicial";

		return view('index.php', $data);
	}

	public function login()
	{
		$data ['titulo'] = "CurtURL's - Cadastro";

		return view('login.php', $data);
	}

	public function cadastro()
	{
		$data ['titulo'] = "CurtURL's - Cadastro";

		return view('cadastro.php', $data);
	}

	public function inserir()
	{
		$nome = $this->request->getPost('nome');
		$email = $this->request->getPost('email');
		$senha = $this->request->getPost('senha');

		$dados = [
			'nome' => $nome,
			'email' => $email,
			'senha' => $senha
		];

		$usuarioModel = new \App\Models\user_model();

		$usuarioModel->insert($dados);

		echo "inserido com sucesso";

		echo "Usuário cadastrado, seja bem-vindo(a) $nome";
	}

	public function listar()
	{
		echo "listar";
	}

	//--------------------------------------------------------------------

}
